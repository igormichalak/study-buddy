// import logo from 'assets/logo.svg';
import UsersList from 'components/UsersList/UsersList';
import 'views/App.css';

import { users as data } from 'data/users';

function App() {
	return <UsersList users={data} />;
}

export default App;
