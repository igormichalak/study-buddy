import React from 'react';
import PropTypes from 'prop-types';
import './UsersListItem.css';

const UsersListItem = ({ userData: { name, age } }) => {
	return (
		<li className="userListItem">
			{name}
			{age ? `, ${age}` : null}
		</li>
	);
};

UsersListItem.propTypes = {
	userData: PropTypes.shape({
		name: PropTypes.string.isRequired,
		age: PropTypes.number,
	}),
};

export default UsersListItem;
