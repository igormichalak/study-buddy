import React from 'react';
import UsersListItem from 'components/UsersListItem/UsersListItem';
import PropTypes from 'prop-types';
import './UsersList.css';

const UsersList = ({ users }) => {
	return (
		<div>
			<ul className="usersList">
				{users
					? users.map((userData) => (
							<UsersListItem key={userData.name} userData={userData} />
					  ))
					: null}
			</ul>
		</div>
	);
};

UsersList.propTypes = {
	users: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string.isRequired,
			age: PropTypes.number,
		})
	),
};

export default UsersList;
